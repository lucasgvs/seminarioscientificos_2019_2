package br.com.mauda.seminario.cientificos.model;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao {

    private Long id;
    private Boolean direitoMaterial;
    private Seminario seminario;
    private Estudante estudante;
    private SituacaoInscricaoEnum situacao = SituacaoInscricaoEnum.DISPONIVEL;

    public Inscricao(Seminario seminario) {
        this.seminario = seminario;
        this.seminario.getInscricoes().add(this);
    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        this.estudante = estudante;
        this.direitoMaterial = direitoMaterial;
        this.estudante.adicionarInscricao(this);
        this.situacao = SituacaoInscricaoEnum.COMPRADO;
    }

    public void realizarCheckIn() {
        this.situacao = SituacaoInscricaoEnum.CHECKIN;
    }

    public void cancelarCompra() {
        this.estudante.removerInscricao(this);
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
        this.direitoMaterial = null;
        this.estudante = null;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
