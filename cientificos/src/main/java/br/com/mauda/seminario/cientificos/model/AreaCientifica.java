package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;

public class AreaCientifica {

    private Long id;
    private String nome;
    private java.util.List<Curso> cursos = new ArrayList<>();

    public void adicionarCurso(Curso curso) {
        this.cursos.add(curso);
    }

    public boolean possuiCurso(Curso curso) {
        return this.cursos.contains(curso);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public java.util.List<Curso> getCursos() {
        return this.cursos;
    }
}
